﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLCLient
{
    public partial class loan_table : Form
    {
        public loan_table()
        {
            InitializeComponent();
        }

        private void loan_table_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'libraryDataSet.loan' table. You can move, or remove it, as needed.
            this.loanTableAdapter.Fill(this.libraryDataSet.loan);

        }

        private void OtherButton_Click(object sender, EventArgs e)
        {
            other_functions form = new other_functions();
            form.Show();
        }
    }
}
