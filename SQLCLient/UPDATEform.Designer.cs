﻿namespace SQLCLient
{
    partial class UPDATEform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UPDATEbox = new System.Windows.Forms.ComboBox();
            this.SELECT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UPDATEtextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Execute = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // UPDATEbox
            // 
            this.UPDATEbox.FormattingEnabled = true;
            this.UPDATEbox.Items.AddRange(new object[] {
            "dbo.adult ",
            "dbo.copy ",
            "dbo.item ",
            "dbo.juvenile ",
            "dbo.loan ",
            "dbo.loanhist ",
            "dbo.member ",
            "dbo.reservation ",
            "dbo.title"});
            this.UPDATEbox.Location = new System.Drawing.Point(100, 109);
            this.UPDATEbox.Name = "UPDATEbox";
            this.UPDATEbox.Size = new System.Drawing.Size(181, 24);
            this.UPDATEbox.TabIndex = 4;
            // 
            // SELECT
            // 
            this.SELECT.AutoSize = true;
            this.SELECT.Font = new System.Drawing.Font("Consolas", 11F);
            this.SELECT.Location = new System.Drawing.Point(24, 111);
            this.SELECT.Name = "SELECT";
            this.SELECT.Size = new System.Drawing.Size(70, 22);
            this.SELECT.TabIndex = 5;
            this.SELECT.Text = "UPDATE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11F);
            this.label1.Location = new System.Drawing.Point(287, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "SET";
            // 
            // UPDATEtextbox
            // 
            this.UPDATEtextbox.Location = new System.Drawing.Point(333, 112);
            this.UPDATEtextbox.Name = "UPDATEtextbox";
            this.UPDATEtextbox.Size = new System.Drawing.Size(261, 22);
            this.UPDATEtextbox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 11F);
            this.label2.Location = new System.Drawing.Point(600, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 22);
            this.label2.TabIndex = 10;
            this.label2.Text = "WHERE";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(666, 113);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(261, 22);
            this.textBox1.TabIndex = 11;
            // 
            // Execute
            // 
            this.Execute.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.Execute.Location = new System.Drawing.Point(345, 156);
            this.Execute.Name = "Execute";
            this.Execute.Size = new System.Drawing.Size(261, 31);
            this.Execute.TabIndex = 12;
            this.Execute.Text = "Execute";
            this.Execute.UseVisualStyleBackColor = true;
            this.Execute.Click += new System.EventHandler(this.Execute_Click);
            // 
            // UPDATEform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 288);
            this.Controls.Add(this.Execute);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.UPDATEtextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SELECT);
            this.Controls.Add(this.UPDATEbox);
            this.MaximumSize = new System.Drawing.Size(975, 335);
            this.MinimumSize = new System.Drawing.Size(975, 335);
            this.Name = "UPDATEform";
            this.Text = "UPDATE Query";
            this.Load += new System.EventHandler(this.UPDATEform_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox UPDATEbox;
        private System.Windows.Forms.Label SELECT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UPDATEtextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Execute;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
    }
}