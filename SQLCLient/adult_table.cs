﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLCLient
{
    public partial class adult_table : Form
    {
        public adult_table()
        {
            InitializeComponent();
        }

        private void adult_table_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'libraryDataSet.adult' table. You can move, or remove it, as needed.
            this.adultTableAdapter.Fill(this.libraryDataSet.adult);

        }

        private void CopyTable_Click(object sender, EventArgs e)
        {
            copy_table table = new copy_table();
            table.Show();
        }

        private void ItemTable_Click(object sender, EventArgs e)
        {
            item_table table = new item_table();
            table.Show();
        }

        private void JuvenileTable_Click(object sender, EventArgs e)
        {
            juvenile_table table = new juvenile_table();
            table.Show();
        }

        private void LoanTable_Click(object sender, EventArgs e)
        {
            loan_table table = new loan_table();
            table.Show();
        }

        private void LoanhistTable_Click(object sender, EventArgs e)
        {
            loanhist_table table = new loanhist_table();
            table.Show();
        }

        private void MemberTable_Click(object sender, EventArgs e)
        {
            member_table table = new member_table();
            table.Show();
        }

        private void ReservationTable_Click(object sender, EventArgs e)
        {
            reservation_table table = new reservation_table();
            table.Show();
        }

        private void TitleTable_Click(object sender, EventArgs e)
        {
            title_table table = new title_table();
            table.Show();
        }

        private void OtherButton_Click(object sender, EventArgs e)
        {
            other_functions form = new other_functions();
            form.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigator1_RefreshItems(object sender, EventArgs e)
        {

        }
    }
}
