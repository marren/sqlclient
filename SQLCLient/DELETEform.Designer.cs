﻿namespace SQLCLient
{
    partial class DELETEform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DELETEbox = new System.Windows.Forms.ComboBox();
            this.SELECT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DELETEtextbox = new System.Windows.Forms.TextBox();
            this.Execute = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // DELETEbox
            // 
            this.DELETEbox.FormattingEnabled = true;
            this.DELETEbox.Items.AddRange(new object[] {
            "dbo.adult ",
            "dbo.copy ",
            "dbo.item ",
            "dbo.juvenile ",
            "dbo.loan ",
            "dbo.loanhist ",
            "dbo.member ",
            "dbo.reservation ",
            "dbo.title"});
            this.DELETEbox.Location = new System.Drawing.Point(151, 87);
            this.DELETEbox.Name = "DELETEbox";
            this.DELETEbox.Size = new System.Drawing.Size(181, 24);
            this.DELETEbox.TabIndex = 3;
            // 
            // SELECT
            // 
            this.SELECT.AutoSize = true;
            this.SELECT.Font = new System.Drawing.Font("Consolas", 11F);
            this.SELECT.Location = new System.Drawing.Point(25, 86);
            this.SELECT.Name = "SELECT";
            this.SELECT.Size = new System.Drawing.Size(120, 22);
            this.SELECT.TabIndex = 4;
            this.SELECT.Text = "DELETE from";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11F);
            this.label1.Location = new System.Drawing.Point(338, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 22);
            this.label1.TabIndex = 5;
            this.label1.Text = "where";
            // 
            // DELETEtextbox
            // 
            this.DELETEtextbox.Location = new System.Drawing.Point(404, 90);
            this.DELETEtextbox.Name = "DELETEtextbox";
            this.DELETEtextbox.Size = new System.Drawing.Size(261, 22);
            this.DELETEtextbox.TabIndex = 6;
            // 
            // Execute
            // 
            this.Execute.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.Execute.Location = new System.Drawing.Point(236, 139);
            this.Execute.Name = "Execute";
            this.Execute.Size = new System.Drawing.Size(261, 31);
            this.Execute.TabIndex = 7;
            this.Execute.Text = "Execute";
            this.Execute.UseVisualStyleBackColor = true;
            this.Execute.Click += new System.EventHandler(this.Execute_Click);
            // 
            // DELETEform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 263);
            this.Controls.Add(this.Execute);
            this.Controls.Add(this.DELETEtextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SELECT);
            this.Controls.Add(this.DELETEbox);
            this.MaximumSize = new System.Drawing.Size(730, 310);
            this.MinimumSize = new System.Drawing.Size(730, 310);
            this.Name = "DELETEform";
            this.Text = "DELETE Query";
            this.Load += new System.EventHandler(this.DELETEform_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox DELETEbox;
        private System.Windows.Forms.Label SELECT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DELETEtextbox;
        private System.Windows.Forms.Button Execute;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
    }
}