﻿namespace SQLCLient
{
    partial class other_functions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SELECTbutton = new System.Windows.Forms.Button();
            this.INSERTbutton = new System.Windows.Forms.Button();
            this.DELETEbutton = new System.Windows.Forms.Button();
            this.UPDATEbutton = new System.Windows.Forms.Button();
            this.MakeTXTbutton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // SELECTbutton
            // 
            this.SELECTbutton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.SELECTbutton.Location = new System.Drawing.Point(147, 57);
            this.SELECTbutton.Name = "SELECTbutton";
            this.SELECTbutton.Size = new System.Drawing.Size(185, 40);
            this.SELECTbutton.TabIndex = 20;
            this.SELECTbutton.Text = "SELECT Query";
            this.SELECTbutton.UseVisualStyleBackColor = true;
            this.SELECTbutton.Click += new System.EventHandler(this.SELECTbutton_Click);
            // 
            // INSERTbutton
            // 
            this.INSERTbutton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.INSERTbutton.Location = new System.Drawing.Point(147, 103);
            this.INSERTbutton.Name = "INSERTbutton";
            this.INSERTbutton.Size = new System.Drawing.Size(185, 40);
            this.INSERTbutton.TabIndex = 21;
            this.INSERTbutton.Text = "INSERT Query";
            this.INSERTbutton.UseVisualStyleBackColor = true;
            this.INSERTbutton.Click += new System.EventHandler(this.INSERTbutton_Click);
            // 
            // DELETEbutton
            // 
            this.DELETEbutton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.DELETEbutton.Location = new System.Drawing.Point(147, 149);
            this.DELETEbutton.Name = "DELETEbutton";
            this.DELETEbutton.Size = new System.Drawing.Size(185, 40);
            this.DELETEbutton.TabIndex = 22;
            this.DELETEbutton.Text = "DELETE Query";
            this.DELETEbutton.UseVisualStyleBackColor = true;
            this.DELETEbutton.Click += new System.EventHandler(this.DELETEbutton_Click);
            // 
            // UPDATEbutton
            // 
            this.UPDATEbutton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.UPDATEbutton.Location = new System.Drawing.Point(147, 195);
            this.UPDATEbutton.Name = "UPDATEbutton";
            this.UPDATEbutton.Size = new System.Drawing.Size(185, 40);
            this.UPDATEbutton.TabIndex = 23;
            this.UPDATEbutton.Text = "UPDATE Query";
            this.UPDATEbutton.UseVisualStyleBackColor = true;
            this.UPDATEbutton.Click += new System.EventHandler(this.UPDATEbutton_Click);
            // 
            // MakeTXTbutton
            // 
            this.MakeTXTbutton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.MakeTXTbutton.Location = new System.Drawing.Point(147, 241);
            this.MakeTXTbutton.Name = "MakeTXTbutton";
            this.MakeTXTbutton.Size = new System.Drawing.Size(185, 40);
            this.MakeTXTbutton.TabIndex = 24;
            this.MakeTXTbutton.Text = "Make .txt file";
            this.MakeTXTbutton.UseVisualStyleBackColor = true;
            this.MakeTXTbutton.Click += new System.EventHandler(this.MakeTXTbutton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // other_functions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 348);
            this.Controls.Add(this.MakeTXTbutton);
            this.Controls.Add(this.UPDATEbutton);
            this.Controls.Add(this.DELETEbutton);
            this.Controls.Add(this.INSERTbutton);
            this.Controls.Add(this.SELECTbutton);
            this.MaximumSize = new System.Drawing.Size(500, 395);
            this.MinimumSize = new System.Drawing.Size(500, 395);
            this.Name = "other_functions";
            this.Text = "SQLClient: Other";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SELECTbutton;
        private System.Windows.Forms.Button INSERTbutton;
        private System.Windows.Forms.Button DELETEbutton;
        private System.Windows.Forms.Button UPDATEbutton;
        private System.Windows.Forms.Button MakeTXTbutton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}