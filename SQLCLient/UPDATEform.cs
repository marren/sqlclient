﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SQLCLient
{
    public partial class UPDATEform : Form
    {
        public UPDATEform()
        {
            InitializeComponent();
        }

        private void Execute_Click(object sender, EventArgs e)
        {
            string connectString = "Data Source = MARREN; Initial Catalog = library; User ID = SQLClient; Password = password;";
            SqlConnection connection = new SqlConnection(connectString);
            try
            {
                connection.Open(); //відкриття з'єднання
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                SqlCommand update;
                SqlDataAdapter adapter = new SqlDataAdapter();
                String sql;

                sql = $"UPDATE {UPDATEbox.Text} SET {UPDATEtextbox.Text} WHERE {textBox1.Text}"; //запит
                update = new SqlCommand(sql, connection); //оголошення запиту

                adapter.UpdateCommand = new SqlCommand(sql, connection);
                adapter.UpdateCommand.ExecuteNonQuery(); //ExecuteNonQuery() виконує запит та повертає рядки, які були використані
                MessageBox.Show("Succeed.");
                update.Dispose(); //закриття запиту
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            connection.Close(); //закриття з'єднання
        }

        private void UPDATEform_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(UPDATEbox, "Select a table");
            toolTip2.SetToolTip(UPDATEtextbox, "column1 = value1, column2 = value2, ...");
            toolTip3.SetToolTip(textBox1, "Type a condition");
        }
    }
}