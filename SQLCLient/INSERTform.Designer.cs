﻿namespace SQLCLient
{
    partial class INSERTform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SELECT = new System.Windows.Forms.Label();
            this.INSERTbox = new System.Windows.Forms.ComboBox();
            this.INSERTtextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Execute = new System.Windows.Forms.Button();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // SELECT
            // 
            this.SELECT.AutoSize = true;
            this.SELECT.Font = new System.Drawing.Font("Consolas", 11F);
            this.SELECT.Location = new System.Drawing.Point(12, 78);
            this.SELECT.Name = "SELECT";
            this.SELECT.Size = new System.Drawing.Size(120, 22);
            this.SELECT.TabIndex = 1;
            this.SELECT.Text = "INSERT into";
            // 
            // INSERTbox
            // 
            this.INSERTbox.FormattingEnabled = true;
            this.INSERTbox.Items.AddRange(new object[] {
            "dbo.adult ",
            "dbo.copy ",
            "dbo.item ",
            "dbo.juvenile ",
            "dbo.loan ",
            "dbo.loanhist ",
            "dbo.member ",
            "dbo.reservation ",
            "dbo.title"});
            this.INSERTbox.Location = new System.Drawing.Point(138, 78);
            this.INSERTbox.Name = "INSERTbox";
            this.INSERTbox.Size = new System.Drawing.Size(181, 24);
            this.INSERTbox.TabIndex = 3;
            this.INSERTbox.SelectedIndexChanged += new System.EventHandler(this.INSERTbox_SelectedIndexChanged);
            // 
            // INSERTtextbox
            // 
            this.INSERTtextbox.Location = new System.Drawing.Point(325, 80);
            this.INSERTtextbox.Name = "INSERTtextbox";
            this.INSERTtextbox.Size = new System.Drawing.Size(261, 22);
            this.INSERTtextbox.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11F);
            this.label1.Location = new System.Drawing.Point(592, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 22);
            this.label1.TabIndex = 10;
            this.label1.Text = "values";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(668, 81);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(261, 22);
            this.textBox1.TabIndex = 11;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Execute
            // 
            this.Execute.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.Execute.Location = new System.Drawing.Point(350, 120);
            this.Execute.Name = "Execute";
            this.Execute.Size = new System.Drawing.Size(261, 31);
            this.Execute.TabIndex = 13;
            this.Execute.Text = "Execute";
            this.Execute.UseVisualStyleBackColor = true;
            this.Execute.Click += new System.EventHandler(this.Execute_Click);
            // 
            // INSERTform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 223);
            this.Controls.Add(this.Execute);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.INSERTtextbox);
            this.Controls.Add(this.INSERTbox);
            this.Controls.Add(this.SELECT);
            this.MaximumSize = new System.Drawing.Size(980, 270);
            this.MinimumSize = new System.Drawing.Size(980, 270);
            this.Name = "INSERTform";
            this.Text = "INSERT Query";
            this.Load += new System.EventHandler(this.INSERTform_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label SELECT;
        private System.Windows.Forms.ComboBox INSERTbox;
        private System.Windows.Forms.TextBox INSERTtextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button Execute;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
    }
}