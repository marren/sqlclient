﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace SQLCLient
{
    public partial class other_functions : Form
    {
        public other_functions()
        {
            InitializeComponent();
        }

        private void SELECTbutton_Click(object sender, EventArgs e)
        {
            SELECTform select = new SELECTform();
            select.Show();
        }

        private void INSERTbutton_Click(object sender, EventArgs e)
        {
            INSERTform form = new INSERTform();
            form.Show();
        }

        private void DELETEbutton_Click(object sender, EventArgs e)
        {
            DELETEform form = new DELETEform();
            form.Show();
        }

        private void UPDATEbutton_Click(object sender, EventArgs e)
        {
            UPDATEform form = new UPDATEform();
            form.Show();
        }

        public void MakeTXTbutton_Click(object sender, EventArgs e)
        {
            BrowseForm form = new BrowseForm();
            form.Show();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
