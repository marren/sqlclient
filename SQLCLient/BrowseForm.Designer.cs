﻿namespace SQLCLient
{
    partial class BrowseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SAVEbutton = new System.Windows.Forms.Button();
            this.SELECT = new System.Windows.Forms.Label();
            this.BrowseBox = new System.Windows.Forms.ComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // SAVEbutton
            // 
            this.SAVEbutton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.SAVEbutton.Location = new System.Drawing.Point(178, 90);
            this.SAVEbutton.Name = "SAVEbutton";
            this.SAVEbutton.Size = new System.Drawing.Size(114, 40);
            this.SAVEbutton.TabIndex = 21;
            this.SAVEbutton.Text = "Save";
            this.SAVEbutton.UseVisualStyleBackColor = true;
            this.SAVEbutton.Click += new System.EventHandler(this.SAVEbutton_Click);
            // 
            // SELECT
            // 
            this.SELECT.AutoSize = true;
            this.SELECT.Font = new System.Drawing.Font("Consolas", 11F);
            this.SELECT.Location = new System.Drawing.Point(12, 38);
            this.SELECT.Name = "SELECT";
            this.SELECT.Size = new System.Drawing.Size(230, 22);
            this.SELECT.TabIndex = 22;
            this.SELECT.Text = "Choose table to save: ";
            // 
            // BrowseBox
            // 
            this.BrowseBox.FormattingEnabled = true;
            this.BrowseBox.Items.AddRange(new object[] {
            "dbo.adult ",
            "dbo.copy ",
            "dbo.item ",
            "dbo.juvenile ",
            "dbo.loan ",
            "dbo.loanhist ",
            "dbo.member ",
            "dbo.reservation ",
            "dbo.title"});
            this.BrowseBox.Location = new System.Drawing.Point(248, 39);
            this.BrowseBox.Name = "BrowseBox";
            this.BrowseBox.Size = new System.Drawing.Size(181, 24);
            this.BrowseBox.TabIndex = 23;
            this.BrowseBox.SelectedIndexChanged += new System.EventHandler(this.BrowseBox_SelectedIndexChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "txt";
            // 
            // BrowseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 163);
            this.Controls.Add(this.BrowseBox);
            this.Controls.Add(this.SELECT);
            this.Controls.Add(this.SAVEbutton);
            this.MaximumSize = new System.Drawing.Size(490, 210);
            this.MinimumSize = new System.Drawing.Size(490, 210);
            this.Name = "BrowseForm";
            this.Text = "Browse...";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SAVEbutton;
        private System.Windows.Forms.Label SELECT;
        private System.Windows.Forms.ComboBox BrowseBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}