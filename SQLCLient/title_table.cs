﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLCLient
{
    public partial class title_table : Form
    {
        public title_table()
        {
            InitializeComponent();
        }

        private void title_table_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'libraryDataSet.title' table. You can move, or remove it, as needed.
            this.titleTableAdapter.Fill(this.libraryDataSet.title);

        }

        private void OtherButton_Click(object sender, EventArgs e)
        {
            other_functions form = new other_functions();
            form.Show();
        }
    }
}
