﻿namespace SQLCLient
{
    partial class SELECTform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SELECT = new System.Windows.Forms.Label();
            this.SELECTbox = new System.Windows.Forms.ComboBox();
            this.libraryDataSet = new SQLCLient.libraryDataSet();
            this.adultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.adultTableAdapter = new SQLCLient.libraryDataSetTableAdapters.adultTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.SELECTtextbox = new System.Windows.Forms.TextBox();
            this.Execute = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.TableBox = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.libraryDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // SELECT
            // 
            this.SELECT.AutoSize = true;
            this.SELECT.Font = new System.Drawing.Font("Consolas", 11F);
            this.SELECT.Location = new System.Drawing.Point(12, 46);
            this.SELECT.Name = "SELECT";
            this.SELECT.Size = new System.Drawing.Size(70, 22);
            this.SELECT.TabIndex = 0;
            this.SELECT.Text = "SELECT";
            // 
            // SELECTbox
            // 
            this.SELECTbox.FormattingEnabled = true;
            this.SELECTbox.Items.AddRange(new object[] {
            "dbo.adult (NOT FOR USE!)",
            "member_no",
            "street",
            "city",
            "state",
            "zip",
            "phone_no",
            "expr_date",
            "",
            "dbo.copy (NOT FOR USE!)",
            "isbn",
            "copy_no",
            "title_no",
            "on_loan",
            "",
            "dbo.item (NOT FOR USE!)",
            "isbn",
            "title_no",
            "translation",
            "cover",
            "loanable",
            "",
            "dbo.juvenile (NOT FOR USE!)",
            "member_no",
            "adult_member_no",
            "birth_date",
            "",
            "dbo.loan (NOT FOR USE!)",
            "isbn",
            "copy_no",
            "title_no",
            "member_no",
            "out_date",
            "due_date",
            "",
            "dbo.loanhist (NOT FOR USE!)",
            "isbn",
            "copy_no",
            "out_date",
            "title_no",
            "member_no",
            "out_date",
            "due_date",
            "in_date",
            "fine_assessed",
            "fine_paid",
            "fine_waived",
            "remarks",
            "",
            "dbo.member (NOT FOR USE!)",
            "member_no",
            "lastname",
            "firstname",
            "middleinitial",
            "photograph",
            "",
            "dbo.reservation (NOT FOR USE!)",
            "isbn",
            "member_no",
            "log_date",
            "remarks",
            "",
            "dbo.title (NOT FOR USE!)",
            "title_no",
            "title",
            "author",
            "synopsis"});
            this.SELECTbox.Location = new System.Drawing.Point(88, 44);
            this.SELECTbox.Name = "SELECTbox";
            this.SELECTbox.Size = new System.Drawing.Size(181, 24);
            this.SELECTbox.TabIndex = 2;
            this.SELECTbox.SelectedIndexChanged += new System.EventHandler(this.SELECTbox_SelectedIndexChanged);
            // 
            // libraryDataSet
            // 
            this.libraryDataSet.DataSetName = "libraryDataSet";
            this.libraryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // adultBindingSource
            // 
            this.adultBindingSource.DataMember = "adult";
            this.adultBindingSource.DataSource = this.libraryDataSet;
            // 
            // adultTableAdapter
            // 
            this.adultTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11F);
            this.label1.Location = new System.Drawing.Point(542, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 22);
            this.label1.TabIndex = 3;
            this.label1.Text = "FROM";
            // 
            // SELECTtextbox
            // 
            this.SELECTtextbox.Location = new System.Drawing.Point(275, 46);
            this.SELECTtextbox.Name = "SELECTtextbox";
            this.SELECTtextbox.Size = new System.Drawing.Size(261, 22);
            this.SELECTtextbox.TabIndex = 4;
            this.SELECTtextbox.TextChanged += new System.EventHandler(this.SELECTtextbox_TextChanged);
            // 
            // Execute
            // 
            this.Execute.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.Execute.Location = new System.Drawing.Point(266, 89);
            this.Execute.Name = "Execute";
            this.Execute.Size = new System.Drawing.Size(261, 31);
            this.Execute.TabIndex = 5;
            this.Execute.Text = "Execute";
            this.Execute.UseVisualStyleBackColor = true;
            this.Execute.Click += new System.EventHandler(this.Execute_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 139);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(757, 355);
            this.dataGridView1.TabIndex = 6;
            // 
            // TableBox
            // 
            this.TableBox.FormattingEnabled = true;
            this.TableBox.Items.AddRange(new object[] {
            "adult",
            "copy",
            "item",
            "juvenile",
            "loan",
            "loanhist",
            "member",
            "reservation",
            "title"});
            this.TableBox.Location = new System.Drawing.Point(598, 46);
            this.TableBox.Name = "TableBox";
            this.TableBox.Size = new System.Drawing.Size(175, 24);
            this.TableBox.TabIndex = 7;
            this.TableBox.SelectedIndexChanged += new System.EventHandler(this.TableBox_SelectedIndexChanged);
            // 
            // SELECTform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 503);
            this.Controls.Add(this.TableBox);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Execute);
            this.Controls.Add(this.SELECTtextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SELECTbox);
            this.Controls.Add(this.SELECT);
            this.MaximumSize = new System.Drawing.Size(807, 550);
            this.MinimumSize = new System.Drawing.Size(807, 550);
            this.Name = "SELECTform";
            this.Text = "SELECTform";
            this.Load += new System.EventHandler(this.SELECTform_Load);
            ((System.ComponentModel.ISupportInitialize)(this.libraryDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SELECT;
        private System.Windows.Forms.ComboBox SELECTbox;
        private libraryDataSet libraryDataSet;
        private System.Windows.Forms.BindingSource adultBindingSource;
        private libraryDataSetTableAdapters.adultTableAdapter adultTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SELECTtextbox;
        private System.Windows.Forms.Button Execute;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox TableBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
    }
}