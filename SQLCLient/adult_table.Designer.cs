﻿namespace SQLCLient
{
    partial class adult_table
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(adult_table));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.membernoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phonenoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exprdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.libraryDataSet = new SQLCLient.libraryDataSet();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.adultTableAdapter = new SQLCLient.libraryDataSetTableAdapters.adultTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.CopyTable = new System.Windows.Forms.Button();
            this.ItemTable = new System.Windows.Forms.Button();
            this.JuvenileTable = new System.Windows.Forms.Button();
            this.LoanTable = new System.Windows.Forms.Button();
            this.LoanhistTable = new System.Windows.Forms.Button();
            this.MemberTable = new System.Windows.Forms.Button();
            this.ReservationTable = new System.Windows.Forms.Button();
            this.TitleTable = new System.Windows.Forms.Button();
            this.OtherButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.libraryDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.membernoDataGridViewTextBoxColumn,
            this.streetDataGridViewTextBoxColumn,
            this.cityDataGridViewTextBoxColumn,
            this.stateDataGridViewTextBoxColumn,
            this.zipDataGridViewTextBoxColumn,
            this.phonenoDataGridViewTextBoxColumn,
            this.exprdateDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.adultBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(13, 76);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1194, 450);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // membernoDataGridViewTextBoxColumn
            // 
            this.membernoDataGridViewTextBoxColumn.DataPropertyName = "member_no";
            this.membernoDataGridViewTextBoxColumn.HeaderText = "member_no";
            this.membernoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.membernoDataGridViewTextBoxColumn.Name = "membernoDataGridViewTextBoxColumn";
            this.membernoDataGridViewTextBoxColumn.Width = 125;
            // 
            // streetDataGridViewTextBoxColumn
            // 
            this.streetDataGridViewTextBoxColumn.DataPropertyName = "street";
            this.streetDataGridViewTextBoxColumn.HeaderText = "street";
            this.streetDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.streetDataGridViewTextBoxColumn.Name = "streetDataGridViewTextBoxColumn";
            this.streetDataGridViewTextBoxColumn.Width = 125;
            // 
            // cityDataGridViewTextBoxColumn
            // 
            this.cityDataGridViewTextBoxColumn.DataPropertyName = "city";
            this.cityDataGridViewTextBoxColumn.HeaderText = "city";
            this.cityDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.cityDataGridViewTextBoxColumn.Name = "cityDataGridViewTextBoxColumn";
            this.cityDataGridViewTextBoxColumn.Width = 125;
            // 
            // stateDataGridViewTextBoxColumn
            // 
            this.stateDataGridViewTextBoxColumn.DataPropertyName = "state";
            this.stateDataGridViewTextBoxColumn.HeaderText = "state";
            this.stateDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.stateDataGridViewTextBoxColumn.Name = "stateDataGridViewTextBoxColumn";
            this.stateDataGridViewTextBoxColumn.Width = 125;
            // 
            // zipDataGridViewTextBoxColumn
            // 
            this.zipDataGridViewTextBoxColumn.DataPropertyName = "zip";
            this.zipDataGridViewTextBoxColumn.HeaderText = "zip";
            this.zipDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.zipDataGridViewTextBoxColumn.Name = "zipDataGridViewTextBoxColumn";
            this.zipDataGridViewTextBoxColumn.Width = 125;
            // 
            // phonenoDataGridViewTextBoxColumn
            // 
            this.phonenoDataGridViewTextBoxColumn.DataPropertyName = "phone_no";
            this.phonenoDataGridViewTextBoxColumn.HeaderText = "phone_no";
            this.phonenoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.phonenoDataGridViewTextBoxColumn.Name = "phonenoDataGridViewTextBoxColumn";
            this.phonenoDataGridViewTextBoxColumn.Width = 125;
            // 
            // exprdateDataGridViewTextBoxColumn
            // 
            this.exprdateDataGridViewTextBoxColumn.DataPropertyName = "expr_date";
            this.exprdateDataGridViewTextBoxColumn.HeaderText = "expr_date";
            this.exprdateDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.exprdateDataGridViewTextBoxColumn.Name = "exprdateDataGridViewTextBoxColumn";
            this.exprdateDataGridViewTextBoxColumn.Width = 125;
            // 
            // adultBindingSource
            // 
            this.adultBindingSource.DataMember = "adult";
            this.adultBindingSource.DataSource = this.libraryDataSet;
            // 
            // libraryDataSet
            // 
            this.libraryDataSet.DataSetName = "libraryDataSet";
            this.libraryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.BindingSource = this.adultBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.adultBindingSource, "member_no", true));
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(1222, 27);
            this.bindingNavigator1.TabIndex = 9;
            this.bindingNavigator1.Text = "bindingNavigator1";
            this.bindingNavigator1.RefreshItems += new System.EventHandler(this.bindingNavigator1_RefreshItems);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // adultTableAdapter
            // 
            this.adultTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.button1.Location = new System.Drawing.Point(13, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 40);
            this.button1.TabIndex = 10;
            this.button1.Text = "adult";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // CopyTable
            // 
            this.CopyTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.CopyTable.Location = new System.Drawing.Point(133, 30);
            this.CopyTable.Name = "CopyTable";
            this.CopyTable.Size = new System.Drawing.Size(114, 40);
            this.CopyTable.TabIndex = 11;
            this.CopyTable.Text = "copy";
            this.CopyTable.UseVisualStyleBackColor = true;
            this.CopyTable.Click += new System.EventHandler(this.CopyTable_Click);
            // 
            // ItemTable
            // 
            this.ItemTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.ItemTable.Location = new System.Drawing.Point(253, 30);
            this.ItemTable.Name = "ItemTable";
            this.ItemTable.Size = new System.Drawing.Size(114, 40);
            this.ItemTable.TabIndex = 12;
            this.ItemTable.Text = "item";
            this.ItemTable.UseVisualStyleBackColor = true;
            this.ItemTable.Click += new System.EventHandler(this.ItemTable_Click);
            // 
            // JuvenileTable
            // 
            this.JuvenileTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.JuvenileTable.Location = new System.Drawing.Point(373, 30);
            this.JuvenileTable.Name = "JuvenileTable";
            this.JuvenileTable.Size = new System.Drawing.Size(114, 40);
            this.JuvenileTable.TabIndex = 13;
            this.JuvenileTable.Text = "juvenile";
            this.JuvenileTable.UseVisualStyleBackColor = true;
            this.JuvenileTable.Click += new System.EventHandler(this.JuvenileTable_Click);
            // 
            // LoanTable
            // 
            this.LoanTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.LoanTable.Location = new System.Drawing.Point(493, 30);
            this.LoanTable.Name = "LoanTable";
            this.LoanTable.Size = new System.Drawing.Size(114, 40);
            this.LoanTable.TabIndex = 14;
            this.LoanTable.Text = "loan";
            this.LoanTable.UseVisualStyleBackColor = true;
            this.LoanTable.Click += new System.EventHandler(this.LoanTable_Click);
            // 
            // LoanhistTable
            // 
            this.LoanhistTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.LoanhistTable.Location = new System.Drawing.Point(613, 30);
            this.LoanhistTable.Name = "LoanhistTable";
            this.LoanhistTable.Size = new System.Drawing.Size(114, 40);
            this.LoanhistTable.TabIndex = 15;
            this.LoanhistTable.Text = "loanhist";
            this.LoanhistTable.UseVisualStyleBackColor = true;
            this.LoanhistTable.Click += new System.EventHandler(this.LoanhistTable_Click);
            // 
            // MemberTable
            // 
            this.MemberTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.MemberTable.Location = new System.Drawing.Point(733, 30);
            this.MemberTable.Name = "MemberTable";
            this.MemberTable.Size = new System.Drawing.Size(114, 40);
            this.MemberTable.TabIndex = 16;
            this.MemberTable.Text = "member";
            this.MemberTable.UseVisualStyleBackColor = true;
            this.MemberTable.Click += new System.EventHandler(this.MemberTable_Click);
            // 
            // ReservationTable
            // 
            this.ReservationTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.ReservationTable.Location = new System.Drawing.Point(853, 30);
            this.ReservationTable.Name = "ReservationTable";
            this.ReservationTable.Size = new System.Drawing.Size(114, 40);
            this.ReservationTable.TabIndex = 17;
            this.ReservationTable.Text = "reservation";
            this.ReservationTable.UseVisualStyleBackColor = true;
            this.ReservationTable.Click += new System.EventHandler(this.ReservationTable_Click);
            // 
            // TitleTable
            // 
            this.TitleTable.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.TitleTable.Location = new System.Drawing.Point(973, 30);
            this.TitleTable.Name = "TitleTable";
            this.TitleTable.Size = new System.Drawing.Size(114, 40);
            this.TitleTable.TabIndex = 18;
            this.TitleTable.Text = "title";
            this.TitleTable.UseVisualStyleBackColor = true;
            this.TitleTable.Click += new System.EventHandler(this.TitleTable_Click);
            // 
            // OtherButton
            // 
            this.OtherButton.Font = new System.Drawing.Font("Calibri Light", 11F);
            this.OtherButton.Location = new System.Drawing.Point(1093, 30);
            this.OtherButton.Name = "OtherButton";
            this.OtherButton.Size = new System.Drawing.Size(114, 40);
            this.OtherButton.TabIndex = 19;
            this.OtherButton.Text = "Other";
            this.OtherButton.UseVisualStyleBackColor = true;
            this.OtherButton.Click += new System.EventHandler(this.OtherButton_Click);
            // 
            // adult_table
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1222, 538);
            this.Controls.Add(this.OtherButton);
            this.Controls.Add(this.TitleTable);
            this.Controls.Add(this.ReservationTable);
            this.Controls.Add(this.MemberTable);
            this.Controls.Add(this.LoanhistTable);
            this.Controls.Add(this.LoanTable);
            this.Controls.Add(this.JuvenileTable);
            this.Controls.Add(this.ItemTable);
            this.Controls.Add(this.CopyTable);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.dataGridView1);
            this.MaximumSize = new System.Drawing.Size(1240, 585);
            this.MinimumSize = new System.Drawing.Size(1240, 585);
            this.Name = "adult_table";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SQLClient: adult_table";
            this.Load += new System.EventHandler(this.adult_table_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.libraryDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private libraryDataSet libraryDataSet;
        private System.Windows.Forms.BindingSource adultBindingSource;
        private libraryDataSetTableAdapters.adultTableAdapter adultTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn membernoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn streetDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phonenoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn exprdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button CopyTable;
        private System.Windows.Forms.Button ItemTable;
        private System.Windows.Forms.Button JuvenileTable;
        private System.Windows.Forms.Button LoanTable;
        private System.Windows.Forms.Button LoanhistTable;
        private System.Windows.Forms.Button MemberTable;
        private System.Windows.Forms.Button ReservationTable;
        private System.Windows.Forms.Button TitleTable;
        private System.Windows.Forms.Button OtherButton;
    }
}