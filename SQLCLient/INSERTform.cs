﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SQLCLient
{
    public partial class INSERTform : Form
    {
        public INSERTform()
        {
            InitializeComponent();
        }

        private void TableBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void INSERTform_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(INSERTbox, "Select a table");
            toolTip2.SetToolTip(INSERTtextbox, "column1, column2, column3, ...");
            toolTip3.SetToolTip(textBox1, "value1, value2, value3, ...");
        }

        private void Execute_Click(object sender, EventArgs e)
        {
            string connectString = "Data Source = MARREN; Initial Catalog = library; User ID = SQLClient; Password = password;";
            SqlConnection connection = new SqlConnection(connectString);
            try
            {
                connection.Open(); //відкриття з'єднання
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                SqlCommand insert;
                SqlDataAdapter adapter = new SqlDataAdapter();
                String sql;

                sql = $"INSERT INTO {INSERTbox.Text} ({INSERTtextbox.Text}) VALUES ({textBox1.Text})"; //запит
                insert = new SqlCommand(sql, connection); //оголошення запиту

                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.ExecuteNonQuery(); //ExecuteNonQuery() виконує запит та повертає рядки, які були використані
                MessageBox.Show("Succeed.");
                insert.Dispose(); //закриття запиту
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            connection.Close(); //закриття з'єднання
        }

        private void INSERTbox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
