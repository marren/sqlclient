﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SQLCLient
{
    public partial class SELECTform : Form
    {
        public SELECTform()
        {
            InitializeComponent();
        }

        private void SELECTform_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'libraryDataSet.adult' table. You can move, or remove it, as needed.
            this.adultTableAdapter.Fill(this.libraryDataSet.adult);
            toolTip1.SetToolTip(SELECTbox, "Select a column");
            toolTip2.SetToolTip(SELECTtextbox, "Enter more column names (, column2, column3...)");
            toolTip3.SetToolTip(TableBox, "Select a table");
        }

        private void SELECTbox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void SELECTtextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Execute_Click(object sender, EventArgs e)
        {
            string connectString = "Data Source = MARREN; Initial Catalog = library; User ID = SQLClient; Password = password;";
            SqlConnection connection = new SqlConnection(connectString);
            try
            {
                connection.Open(); //відкриття з'єднання
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand select;
            SqlDataReader dataReader;
            String sql;

            sql = $"SELECT {SELECTbox.Text} {SELECTtextbox.Text} FROM {TableBox.Text}"; //запит
            select = new SqlCommand(sql, connection);

            try
            {
                dataReader = select.ExecuteReader();
                
                //отримання об'єкта для читання
                List<string[]> data = new List<string[]>(); //список для збереження отриманих даних
                string[] columns = new string[dataReader.FieldCount];

                for (int i = 0; i < dataReader.FieldCount; i++)
                    columns[i] = dataReader.GetName(i); //записуємо заголовки стовпців

                for (int i = 0; i < columns.Length; i++)
                {
                    dataGridView1.ColumnCount = columns.Length;
                    dataGridView1.Columns[i].Name = $"{columns[i]}"; //створення стовпців
                }

                while (dataReader.Read())
                {
                    data.Add(new string[dataReader.FieldCount]); //кількість стовпців
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        data[data.Count - 1][i] = dataReader[i].ToString(); //заповнюємо список даними
                    }
                }
                dataReader.Close();

                foreach (string[] s in data)
                    dataGridView1.Rows.Add(s); //додаємо нові рядки в DataGridView
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            connection.Close();
        }

        private void TableBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
