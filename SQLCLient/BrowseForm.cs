﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SQLCLient
{
    public partial class BrowseForm : Form
    {
        public BrowseForm()
        {
            InitializeComponent();
        }

        private void SAVEbutton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.ShowDialog();

                string savePath = Path.GetDirectoryName(saveFile.FileName);
                if (saveFile.ShowDialog() == DialogResult.OK)
                    savePath = Path.GetDirectoryName(saveFile.FileName);
                File.WriteAllText(savePath, BrowseBox.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BrowseBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
