﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SQLCLient
{
    public partial class DELETEform : Form
    {
        public DELETEform()
        {
            InitializeComponent();
        }

        private void Execute_Click(object sender, EventArgs e)
        {
            string connectString = "Data Source = MARREN; Initial Catalog = library; User ID = SQLClient; Password = password;";
            SqlConnection connection = new SqlConnection(connectString);
            try
            {
                connection.Open(); //відкриття з'єднання
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand delete;
            SqlDataAdapter adapter = new SqlDataAdapter();
            String sql;

            sql = $"DELETE from {DELETEbox.Text} where {DELETEtextbox.Text}"; //запит

            delete = new SqlCommand(sql, connection); //оголошення запиту

            try
            {
                adapter.DeleteCommand = new SqlCommand(sql, connection);
                adapter.DeleteCommand.ExecuteNonQuery(); //ExecuteNonQuery() виконує запит та повертає рядки, які були використані
                MessageBox.Show("Succeed.");
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

            delete.Dispose(); //закриття запиту
            connection.Close(); //закриття з'єднання
        }

        private void DELETEform_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(DELETEbox, "Select a table");
            toolTip2.SetToolTip(DELETEtextbox, "Type a condition");
        }
    }
}
