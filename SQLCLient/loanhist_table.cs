﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLCLient
{
    public partial class loanhist_table : Form
    {
        public loanhist_table()
        {
            InitializeComponent();
        }

        private void loanhist_table_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'libraryDataSet.loanhist' table. You can move, or remove it, as needed.
            this.loanhistTableAdapter.Fill(this.libraryDataSet.loanhist);

        }

        private void OtherButton_Click(object sender, EventArgs e)
        {
            other_functions form = new other_functions();
            form.Show();
        }
    }
}
